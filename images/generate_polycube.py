from math import pi
import bpy
import json
import sys
import copy
from itertools import permutations, product

# Utils
class Polycube(object):

    def __init__(self, json_obj):
        self.cells = set(tuple(cell) for cell in json_obj['cells'])
        if 'roots' in json_obj:
            self.roots = [tuple(r) for r in json_obj['roots']]
        else:
            self.roots = [tuple(json_obj['root'])]
        if 'directions' in json_obj:
            self.directions = [tuple(direction)\
                               for direction in json_obj['directions']]
        else:
            self.directions = [tuple(json_obj['direction'])]

    def apply_permutation(self, permutation, signs=[1,1,1]):
        (p1,p2,p3) = permutation
        (s1,s2,s3) = signs
        self.cells      = [(s1 * c[p1], s2 * c[p2], s3 * c[p3])\
                           for c in self.cells]
        self.roots      = [(s1 * r[p1], s2 * r[p2], s3 * r[p3])\
                           for r in self.roots]
        self.directions = [(s1 * d[p1], s2 * d[p2], s3 * d[p3])\
                           for d in self.directions]
        self.cells = set(self.cells)

    def rotation_euler(self):
        (dx,dy,dz) = self.directions[0]
        x = 0
        y = 0
        if   dx == 1:  y = pi/2
        elif dx == -1: y = -pi/2
        if   dy == 1:  x = -pi/2
        elif dy == -1: x = pi/2

        if   dz == -1:
            x += pi
        return [x, y, 0]

    def canonize(self):
        if self.depth() > self.width():
            self.apply_permutation([1,0,2])
        if self.height() > self.width():
            self.apply_permutation([2,1,0])
        if self.directions[0][1] != -1:
            self.apply_permutation([0,1,2],[1,-1,1])
        self.cells = set(self.cells)

    def __iter__(self):
        return iter(self.cells)

    def degree(self, cell):
        return len(neighborhood(cell) & self.cells)

    def min_x(self):
        return min(cell[0] for cell in self.cells)
    
    def max_x(self):
        return max(cell[0] for cell in self.cells)
    
    def min_y(self):
        return min(cell[1] for cell in self.cells)
    
    def max_y(self):
        return max(cell[1] for cell in self.cells)
    
    def min_z(self):
        return min(cell[2] for cell in self.cells)
    
    def max_z(self):
        return max(cell[2] for cell in self.cells)
    
    def width(self):
        return self.max_x() - self.min_x() + 1
    
    def height(self):
        return self.max_z() - self.min_z() + 1
    
    def depth(self):
        return self.max_y() - self.min_y() + 1

def neighborhood(cell):
    (x,y,z) = cell
    return set([(x + 1, y, z), (x - 1, y, z),\
                (x, y + 1, z), (x, y - 1, z),\
                (x, y, z + 1), (x, y, z - 1)])

def hex_to_tuple(s):
    (r,g,b) = (s[1:3],s[3:5],s[5:7])
    return (int(r, base=16) / 256.0,
            int(g, base=16) / 256.0,
            int(b, base=16) / 256.0)


# Constants
DEGREE_COLOR = ["#A3BE8C",
                "#EBCB8B",
                "#88C0D0",
                "#D08770",
                "#B48EAD",
                "#BF616A"]

def render_polycube(polycube, output_filename):
    # Cleaning default scene
    bpy.ops.object.select_all(action='DESELECT')
    for o in bpy.data.objects:
        if o.name != 'Camera': o.select = True
    bpy.ops.object.delete()
    
    # Materials
    degree_material = [bpy.data.materials.new('Degree%sMaterial' % d)\
                       for d in range(1, 7)]
    for d in range(6):
        degree_material[d].diffuse_color = hex_to_tuple(DEGREE_COLOR[d])
    arrow_material = bpy.data.materials.new('ArrowMaterial')
    arrow_material.diffuse_color = (0.0, 0.8, 1.0)
    
    polycube.canonize()
    if with_direction == 'yes':
        for (d,(root,direction)) in enumerate(zip(polycube.roots, polycube.directions)):
            arrow_location = (root[0] + direction[0],
                              root[1] + direction[1],
                              root[2] + direction[2])
            bpy.ops.mesh.primitive_cylinder_add(location=arrow_location)
            bpy.context.active_object.name = 'Arrow.%s' % d
            bpy.context.scene.objects.active.scale = (0.05, 0.05, 1.3)
            bpy.context.scene.objects.active.rotation_mode = 'XYZ'
            bpy.context.scene.objects.active.rotation_euler = polycube.rotation_euler()
            bpy.context.active_object.data.materials.append(arrow_material)
            tip_location = (root[0] + 2.5 * direction[0],
                            root[1] + 2.5 * direction[1],
                            root[2] + 2.5 * direction[2])
            bpy.ops.mesh.primitive_cone_add(location=tip_location)
            bpy.context.active_object.name = 'Tip.%s' % d
            bpy.context.scene.objects.active.scale = (0.2, 0.2, 0.3)
            bpy.context.scene.objects.active.rotation_mode = 'XYZ'
            bpy.context.scene.objects.active.rotation_euler = polycube.rotation_euler()
            bpy.context.active_object.data.materials.append(arrow_material)
    for cell in polycube:
        bpy.ops.mesh.primitive_cube_add(location=cell)
        bpy.context.active_object.name = 'Cell.%s.%s.%s' % tuple(cell)
        bpy.context.scene.objects.active.scale = (0.5, 0.5, 0.5)
        d = polycube.degree(cell)
        bpy.context.active_object.data.materials.append(degree_material[d - 1])
    
    # Joining cells
    bpy.ops.object.select_all(action='DESELECT')
    for cell in polycube:
        bpy.data.objects['Cell.%s.%s.%s' % tuple(cell)].select = True
    if with_direction == 'yes':
        for i in range(len(polycube.roots)):
            bpy.data.objects['Arrow.%s' % i].select = True
            bpy.data.objects['Tip.%s' % i].select = True
    bpy.ops.object.join()
    bpy.context.active_object.name = 'Polycube'
    polycube_obj = bpy.data.objects['Polycube']
    polycube_obj.rotation_mode = 'XYZ'
    polycube_obj.rotation_euler[2] = -20 * pi / 180.0
    
    # Get some stats on polycube
    w = polycube.width()
    h = polycube.height()
    
    # Set render options
    scene = bpy.data.scenes['Scene']
    scene.render.resolution_x = w * 200.0
    scene.render.resolution_y = (h + 1) * 200.0
    scene.render.use_freestyle = True
    scene.render.line_thickness = 0.5
    scene.render.alpha_mode = 'TRANSPARENT'
    
    # Camera
    camera = bpy.data.cameras['Camera']
    camera.type = 'ORTHO'
    camera.ortho_scale = 18.0
    camera = bpy.data.objects['Camera']
    camera.location = (polycube.min_x() + 0.5 * w - 0.5,
                       -10,
                       polycube.min_z() + 0.8 * h)
    camera.rotation_mode = 'XYZ'
    camera.rotation_euler = (75 * pi / 180.0, 0, 0)
    
    # Light
    sun_data = bpy.data.lamps.new(name="Sun", type='SUN')
    sun = bpy.data.objects.new(name="Sun", object_data=sun_data)
    scene.objects.link(sun)
    sun.location = (10.0, -10.0, 10.0)
    sun.rotation_mode = 'XYZ'
    sun.rotation_euler = (60 * pi / 180.0, 30 * pi / 180.0, 40 * pi / 180.0)
    
    # Saving the scene
    #bpy.ops.wm.save_as_mainfile(filepath='polycube.blend')
    bpy.data.scenes['Scene'].render.filepath = output_filename
    bpy.ops.render.render(write_still=True)

# ---- #
# Main #
# ---- #
argv = sys.argv[sys.argv.index("--") + 1:]
polycube_type = argv[0]
final_allowed = False
nonfinal_allowed = False
if polycube_type in "af":
    final_allowed = True
if polycube_type in "an":
    nonfinal_allowed = True
prefix = argv[1]
with_direction = argv[2]
six_rotations = argv[3]
json_array = json.loads(sys.stdin.read())

i = 1
for json_obj in json_array:
    if 'recursively-saturated' in json_obj:
        json_obj = json_obj['recursively-saturated']
    if final_allowed and json_obj['final'] or\
            nonfinal_allowed and not json_obj['final']:
        polycube = Polycube(json_obj)
        if six_rotations == 'yes':
            for p in permutations([0,1,2]):
                for s in product([-1,1], repeat=3):
                    polycube.apply_permutation(p, s)
                    render_polycube(polycube, '%s-%s.png' % (prefix, i))
                    i += 1
        else:
            render_polycube(polycube, '%s-%s.png' % (prefix, i))
            i += 1
