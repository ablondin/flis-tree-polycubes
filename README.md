# Fully leafed tree-like polycubes

This repository contains Haskell code that allows the exploration of fully
leafed tree-like polycubes.

It was used as a tool for proving some of the results described in a paper
titled "Saturated Fully Leafed Tree-Like Polyforms and Polycubes", submitted to
the special edition following the [IWOCA
2017](https://carma.newcastle.edu.au/meetings/iwoca/index.html) conference.

## Author

Alexandre Blondin Massé

## Dependencies

- [The Glasgow Haskell Compiler](https://www.haskell.org/ghc/) (the project was
  developed with version 8.2.2);
- [Aeson](https://hackage.haskell.org/package/aeson), an Haskell package that
  handles the JSON format;
- [Aeson-Pretty](https://hackage.haskell.org/package/aeson-pretty), another
  Haskell package built on top of Aeson that handles pretty JSON formatting;
- [Haddock](https://www.haskell.org/haddock/) (optional), to generate the
  documentation;
- [Doctest](https://github.com/sol/doctest) (optional), to test the example in
  the docstrings.
- [Blender](https://www.blender.org/) (optional), to visualize the generated
  polycube.

## Usage

To generate all abundant and sparse branches, it suffices to type
```sh
make abundant
```
The result will be written in the `output` folder. Similarly, the command
```sh
make saturated
```
generates all saturated, recursively saturated and non saturated polycubes,
while the command
```sh
make small
```
generates all saturated polycubes of size at most 25.

Computations can be launched in parallel by editing the value of the variable
`NUM_CORES` in the `Makefile`.

## Playing with the code

First, launch the interpreter with the command
```sh
ghci
```
The [TreePolycubes.hs](TreePolycubes.hs) module can be loaded directly with
```haskell
ghci> :load TreePolycubes.hs
```
Then one can compute all abundant branches of height 2 as follows:
```haskell
ghci> abundantBranches 2
[FreeTree of order 24,FreeTree of order 24,FreeTree of order 6,FreeTree of order 6,FreeTree of order 6,FreeTree of order 6,FreeTree of order 24,FreeTree of order 24,FreeTree of order 12]
```
Since this is not very informative, we can quickly look at the degree
distribution of the inner cells:
```haskell
ghci> map internalDistribution $ abundantBranches 2
[[2,5,6],[2,5,6],[2,5,6],[2,5,6],[2,6,6],[2,6,6],[3,5,5],[3,5,5],[3,5,5]]
```
Note that the computation time increases drastically as the height grows. For
instance, it took about 30 hours of computation on a regular desktop to verify
that the number of abundant branches is finite.

If one does not have 30 hours or more to spend waiting for the program to
terminate, it is possible to consult the JSON files containing the different
branches in the [output](output/) folder. This folder contains all the
abundant, sparse, saturated, recursively saturated and non saturated branches
computed by the program.

## Visualization

We also provide a [Blender](https://www.blender.org/) script that generates
images of the corresponding polycubes. More details can be found in the
[images](images/) folder.

For example, we obtain the following four images when running the script on all
nonfinal abundant branches of height 4 (available in the file
[abundants-04.json](output/abundants-04.json)):

| Polycube                              | Number of cells   | Number of leaves   |
| ------------------------------------- | :---------------: | :----------------: |
| ![](examples/trimmed-abundants-1.png) |  18               | 13                 |
| ![](examples/trimmed-abundants-2.png) |  18               | 13                 |
| ![](examples/trimmed-abundants-3.png) |  21               | 15                 |
| ![](examples/trimmed-abundants-4.png) |  31               | 22                 |


## Documentation

To generate the documentation with [Haddock](https://www.haskell.org/haddock/),
simply type
```sh
make doc
```
and open the file `doc/index.html` in your favorite browser.

## Continuous integration 

Provided [Doctest](https://github.com/sol/doctest) is installed, it is also
possible to test the examples in the docstrings by typing
```sh
make test
```
In principle, all tests should pass on any platform (it was only tested on
MacOS with GHC v.8.2.2). If you obtain failures, please report it to us.

## License

All code contained in this repository is licensed under the [GNU General Public
License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).  All documents and
resources are subjected to the Creative Commons Attribution 4.0 International
License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.
