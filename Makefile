DOC_DIR = doc
HS_FILES = TreePolycubes.hs
NUM_CORES = 1
GHC_OPTIONS = +RTS -N$(NUM_CORES) -s -RTS

.PHONY: small clean compile doc test

compile:
	ghc --make -O2 -threaded -feager-blackholing Main.hs

abundant: compile
	./Main abundant $(GHC_OPTIONS)

saturated: compile
	./Main saturated $(GHC_OPTIONS)

small: compile
	./Main small $(GHC_OPTIONS)

test:
	doctest $(HS_FILES)

doc:
	pandoc -t html README.md -o README.html
	haddock -o $(DOC_DIR) --html $(HS_FILES)

clean:
	rm -rf $(DOC_DIR)
	rm -f *.o *.hi
	rm -f Main

