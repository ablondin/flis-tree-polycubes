import TreePolycubes
import Control.Monad
import Text.Printf
import Data.List (nub)
import Data.Aeson.Encode.Pretty (encodePretty)
import Data.Aeson (toJSON)
import qualified Data.ByteString.Lazy as BSL
import System.Environment

abundantsFilenameFormat = "output/abundants-%2.2d.json"
sparsesFilenameFormat = "output/sparses-%2.2d.json"
minSatFilenameFormat = "output/minimally-saturated-%2.2d.json"
recSatFilenameFormat = "output/recursively-saturated-%2.2d.json"
nonSatFilenameFormat = "output/non-saturated-%2.2d.json"
smallSatFilename = "output/small-saturated.json"

computeAbundants = forM_ [0..11] $ \height -> do
    putStrLn $ "Computing the branches of height " ++ show height
    let (abundants,sparses) = abundantSparseBranches height
    let abundantsFilename = printf abundantsFilenameFormat height
    let sparsesFilename = printf sparsesFilenameFormat height
    putStrLn $ "Writing to file " ++ abundantsFilename
    BSL.writeFile abundantsFilename $ encodePretty $ toJSON abundants
    putStrLn $ "Writing to file " ++ sparsesFilename
    BSL.writeFile sparsesFilename $ encodePretty $ toJSON sparses

computeSaturated = forM_ [0..11] $ \height -> do
    putStrLn $ "Computing saturated branches of height " ++ show height
    let (minSat,recSat,nonSat) = branchesBySaturation height
    let minSatFilename = printf minSatFilenameFormat height
    let recSatFilename = printf recSatFilenameFormat height
    let nonSatFilename = printf nonSatFilenameFormat height
    putStrLn $ "Writing to file " ++ minSatFilename
    BSL.writeFile minSatFilename $ encodePretty $ toJSON minSat
    putStrLn $ "Writing to file " ++ recSatFilename
    BSL.writeFile recSatFilename $ encodePretty $ toJSON recSat
    putStrLn $ "Writing to file " ++ nonSatFilename
    BSL.writeFile nonSatFilename $ encodePretty $ toJSON nonSat

computeSmallSaturated = do
    putStrLn $ "Computing small saturated branches"
    putStrLn $ "Writing to file " ++ smallSatFilename
    BSL.writeFile smallSatFilename $ encodePretty $ toJSON smallSaturated

main = do
    [arg] <- getArgs
    case arg of
        "abundant"  -> do computeAbundants
        "saturated" -> do computeSaturated
        "small"     -> do computeSmallSaturated
