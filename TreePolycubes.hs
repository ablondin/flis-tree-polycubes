{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : TreePolycubes
Description : A module for computing fully leafed tree-like polycubes
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module provides functionalities for computing fully leafed tree-like
polycubes, i.e. tree-like polycubes whose number of leaves is as large as
possible.
 -}
module TreePolycubes where

import Data.List (permutations,nub,sort,elemIndex,partition,find)
import Data.Array
import qualified Data.Set as Set
import Data.Set (Set)
import Data.Maybe (fromJust,catMaybes,isJust,isNothing)
import Data.Aeson (ToJSON,object,toJSON,(.=))

-------------
-- Triples --
-------------

-- | Returns the first value of a triple
tfst :: (a, b, c) -> a
tfst (x,_,_) = x

-- | Returns the second value of a triple
tsnd :: (a, b, c) -> b
tsnd (_,x,_) = x

-- | Returns the third value of a triple
tthd :: (a, b, c) -> c
tthd (_,_,x) = x

------------------------
-- Points and vectors --
------------------------

-- | The 6 unit directions in 3D
data Unit
    -- | The positive x-axis
    = Xp
    -- | The negative x-axis
    | Xm
    -- | The positive y-axis
    | Yp
    -- | The negative y-axis
    | Ym
    -- | The positive z-axis
    | Zp
    -- | The negative z-axis
    | Zm
    deriving (Eq, Show, Ord, Bounded, Enum, Ix)

-- | Returns all units
--
-- >>> units
-- [Xp,Xm,Yp,Ym,Zp,Zm]
units :: [Unit]
units = [Xp .. Zm]

-- | Returns the number of units
--
-- Note: Clearly, it current returns 6, but it could be useful if the ideas are
-- generalized to arbitrary dimensions.
--
-- >>> numUnits
-- 6
numUnits :: Int
numUnits = length units

-- | Returns the bounds of all units
--
-- >>> unitBounds
-- (Xp,Zm)
unitBounds :: (Unit,Unit)
unitBounds = (minBound, maxBound)

-- | Returns the opposite unit
-- 
-- >>> zip units (map opposite units)
-- [(Xp,Xm),(Xm,Xp),(Yp,Ym),(Ym,Yp),(Zp,Zm),(Zm,Zp)]
opposite :: Unit -> Unit
opposite Xp = Xm
opposite Xm = Xp
opposite Yp = Ym
opposite Ym = Yp
opposite Zp = Zm
opposite Zm = Zp

-- | Represents a vector whose entries are integers
--
-- Note: Currently, the program assumes that all vectors are 3D, but this will
-- eventually be generalized.
--
-- >>> Vector [0,-1,0]
-- [0,-1,0]
newtype Vector = Vector [Int]
    deriving (Eq, Ord)

-- | Showing compact vectors
--
-- >>> show $ Vector [0,-1,0]
-- "[0,-1,0]"
instance Show Vector where
    show (Vector v) = show v

-- | Vectors can be seen as numbers
--
-- Note: The most useful operation in this module is @(+)@.
instance Num Vector where
    Vector v1 + Vector v2 = Vector (zipWith (+) v1 v2)
    Vector v1 * Vector v2 = Vector (zipWith (*) v1 v2)
    Vector v1 - Vector v2 = Vector (zipWith (-) v1 v2)
    abs (Vector v)        = Vector (map abs v)
    signum (Vector v)     = Vector (map signum v)
    fromInteger i         = Vector (repeat $ fromInteger i)

-- | The null vector
--
-- >>> zeroVector == Vector [0,0,0]
-- True
zeroVector :: Vector
zeroVector = Vector [0,0,0]

-- | Converts units into vectors
--
-- >>> map vectorFromUnit units
-- [[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]]
vectorFromUnit :: Unit -> Vector
vectorFromUnit Xp = Vector [ 1, 0, 0]
vectorFromUnit Xm = Vector [-1, 0, 0]
vectorFromUnit Yp = Vector [ 0, 1, 0]
vectorFromUnit Ym = Vector [ 0,-1, 0]
vectorFromUnit Zp = Vector [ 0, 0, 1]
vectorFromUnit Zm = Vector [ 0, 0,-1]

-- | Converts vectors into units
--
-- >>> map (unitFromVector . Vector) [[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]]
-- [Xp,Xm,Yp,Ym,Zp,Zm]
unitFromVector :: Vector -> Unit
unitFromVector (Vector [ 1, 0, 0]) = Xp
unitFromVector (Vector [-1, 0, 0]) = Xm
unitFromVector (Vector [ 0, 1, 0]) = Yp
unitFromVector (Vector [ 0,-1, 0]) = Ym
unitFromVector (Vector [ 0, 0, 1]) = Zp
unitFromVector (Vector [ 0, 0,-1]) = Zm
unitFromVector _                   = error "The given vector does not correspond to any unit"

-- | Returns all unit vectors
--
-- >>> unitVectors
-- [[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]]
unitVectors :: [Vector]
unitVectors = map vectorFromUnit units

-- | Returns the neighborhood of a vector.
--
-- The neighborhood of a vector (or point) is the list of all points
-- immediately adjacent to the vector.
--
-- >>> neighborhood (Vector [1,2,-3])
-- fromList [[0,2,-3],[1,1,-3],[1,2,-4],[1,2,-2],[1,3,-3],[2,2,-3]]
neighborhood :: Vector -> Set Vector
neighborhood v = Set.fromList [v + dv | dv <- unitVectors]

-- | Returns the extension of a vector list
--
-- The extension is simply the union of the neighborhoods of all vectors.
--
-- >>> (extension . cellsO) (basicTree "antenna")
-- fromList [[-2,0,-1],[-2,0,1],[-1,-1,-1],[-1,-1,0],[-1,-1,1],[-1,0,-2],[-1,0,-1],[-1,0,0],[-1,0,1],[-1,0,2],[-1,1,-1],[-1,1,1],[0,-2,0],[0,-1,-2],[0,-1,-1],[0,-1,0],[0,-1,1],[0,-1,2],[0,0,-3],[0,0,-2],[0,0,-1],[0,0,0],[0,0,1],[0,0,2],[0,0,3],[0,1,-2],[0,1,-1],[0,1,0],[0,1,1],[0,1,2],[0,2,-1],[0,2,1],[1,-1,-1],[1,-1,0],[1,-1,1],[1,0,-2],[1,0,-1],[1,0,0],[1,0,1],[1,0,2],[1,1,-1],[1,1,1],[2,0,-1],[2,0,1]]
extension :: [Vector] -> Set Vector
extension s = Set.unions [neighborhood c | c <- s]

-- | Returns the extension of a vector list
--
-- The extension is simply the union of the neighborhoods of all vectors.
--
-- >>> (interior . extension . cellsO) (basicTree "antenna")
-- fromList [[-1,0,-1],[-1,0,1],[0,-1,0],[0,0,-2],[0,0,-1],[0,0,0],[0,0,1],[0,0,2],[0,1,-1],[0,1,1],[1,0,-1],[1,0,1]]
interior :: Set Vector -> Set Vector
interior s = Set.filter (s `containsNeigborhoodOf`) s
    where cellsSet `containsNeigborhoodOf` cell = Set.isSubsetOf (neighborhood cell)
                                                                 cellsSet

----------------
-- Isometries --
----------------

-- | A cube isometry
--
-- Note: A cube isometry can be represented by a signed permutation acting on
-- the x, y and z axes. Therefore, there are 48 cube isometries.
--
-- >>> CubeIsometry [0,2,1] [-1,1,1]
-- CubeIsometry [0,2,1] [-1,1,1]
data CubeIsometry = CubeIsometry [Int] [Int]
    deriving (Show)

-- | Returns the list of all cube isometries
--
-- >>> length cubeIsometries
-- 48
cubeIsometries :: [CubeIsometry]
cubeIsometries = [CubeIsometry [x,y,z] [sx,sy,sz]
    | sx <- [-1,1], sy <- [-1,1], sz <- [-1,1],
      [x,y,z] <- permutations [0,1,2]]

-- | Applies an isometry to a unit.
--
-- >>> let f = CubeIsometry [0,2,1] [-1,1,1]
-- >>> zip units $ map (applyIsometry f) units
-- [(Xp,Xm),(Xm,Xp),(Yp,Zp),(Ym,Zm),(Zp,Yp),(Zm,Ym)]
-- >>> let commute f u = applyIsometry f (opposite u) == opposite (applyIsometry f u)
-- >>> and [commute f u | f <- cubeIsometries, u <- units]
-- True
applyIsometry :: CubeIsometry -> Unit -> Unit
applyIsometry (CubeIsometry p s) = unitFromVector . permute . vectorFromUnit
    where permute (Vector v) = Vector $ map (permute' v) [0,1,2]
          permute' v i = (s !! i) * (v !! (p !! i))

-------------------
-- Generic trees --
-------------------

-- | An empty value
--
-- >>> Void
-- Void
-- >>> Void == Void
-- True
data Void = Void
    deriving (Eq, Ord, Show)

-- | A classical tree data structure
data Tree a -- | An empty tree
            = Empty
            -- | A leaf containing anything, and keeping track of the direction of its parent
            | Leaf a Unit
            -- | An internal node containing anything, and keeping track of its children
            | Internal a Unit (Array Unit (Tree a))
    deriving (Eq, Show)

-- | Returns True if and only if the given tree is a leaf
--
-- >>> isEmpty Empty
-- True
-- >>> isEmpty (Leaf Void Xp)
-- False
isEmpty :: Tree a -> Bool
isEmpty Empty = True
isEmpty _     = False

-- | Returns True if and only if the given tree is a leaf
--
-- >>> isLeaf Empty
-- False
-- >>> isLeaf (Leaf Void Xp)
-- True
isLeaf :: Tree a -> Bool
isLeaf (Leaf _ _) = True
isLeaf _          = False

-- | Returns the content of the root of the given tree
--
-- >>> rootContent (Leaf 3 Xp)
-- 3
-- >>> rootContent $ basicTree "6"
-- Void
rootContent :: Tree a -> a
rootContent Empty            = error "The empty tree has not content"
rootContent (Leaf x _)       = x
rootContent (Internal x _ _) = x

-- | Returns the unit of the root of the given tree
--
-- >>> rootUnit (Leaf 3 Xp)
-- Xp
rootUnit :: Tree a -> Unit
rootUnit Empty            = error "The empty tree has no unit"
rootUnit (Leaf _ u)       = u
rootUnit (Internal _ u _) = u

-- | Returns the degree of the root
--
-- >>> map degree basicTrees 
-- [2,2,3,3,3,4,4,4,5,5,6]
degree :: Tree a -> Int
degree Empty             = 0
degree (Leaf _ _)        = 1
degree (Internal _ u ar) = parent + (length $ filter (not . isEmpty) (elems ar))
    where parent = if isEmpty (ar ! u) then 1 else 0

-- | Maps the tree's content to store the degrees
--
-- >>> foldr (:) [] $ degreeTree (basicTree "6")
-- [6,1,1,1,1,1,1]
degreeTree :: Tree a -> Tree Int
degreeTree Empty               = Empty
degreeTree (Leaf _ u)          = Leaf 1 u
degreeTree t@(Internal _ u ar) = Internal (degree t) u (fmap degreeTree ar)

-- | The usual functor view of a tree
--
-- >>> fmap (+3) (Leaf 4 Xp)
-- Leaf 7 Xp
instance Functor Tree where
    fmap _ Empty             = Empty
    fmap f (Leaf x u)        = Leaf (f x) u
    fmap f (Internal x u ar) = Internal (f x) u (fmap (fmap f) ar)

-- | The usual foldable view of a tree
--
-- >>> foldr (+) 0 (Leaf 4 Xp)
-- 4
instance Foldable Tree where
    foldMap _ Empty             = mempty
    foldMap f (Leaf x _)        = f x
    foldMap f (Internal x _ ar) = foldl mappend
                                        (f x)
                                        [foldMap f t | t <- elems ar]

-- | Trees can be ordered
--
-- First, the structure of the tree is checked, with empty trees being smaller
-- than leaves and internal nodes. When the nodes have content, it is compared.
-- Whenever the contents are equal, the rest of the tree is examined
-- recursively.
--
-- Note: A complete proof that this is an order relation should be done, but a
-- quick check suggests that this is indeed the case.
--
-- >>> Empty < Leaf Void Xp
-- True
-- >>> Leaf Void Xp < Empty
-- False
-- >>> Leaf Void Xp < Internal Void Yp (array unitBounds [])
-- True
instance Ord a => Ord (Tree a) where
    Empty              `compare` Empty              = EQ
    Empty              `compare` _                  = LT
    _                  `compare` Empty              = GT
    (Leaf x _)         `compare` (Leaf y _)         = x `compare` y
    (Leaf x _)         `compare` (Internal y _ _)   = if x == y then LT
                                                      else x `compare` y
    (Internal x _ _)   `compare` (Leaf y _)         = if x == y then GT
                                                      else x `compare` y
    (Internal x _ arx) `compare` (Internal y _ ary) =
        foldl mappend
              (x `compare` y)
              [tx `compare` ty | (tx,ty) <- zip (elems arx) (elems ary)]

-- | Algebra on trees allows generic computation of simple statistics
--
-- The four arguments should be used as follows:
--
-- 1. (type `b`) is the zero element of the algebra (the value associated with
--    `Empty`)
-- 2. (type `Bool`) indicates whether the internal nodes should be taken into
--    consideration (`True`) or not (`False`).
-- 3. (type `a -> b`) is the function applied to the content of the nodes.
-- 4. (type `b -> b -> b`) is the function that combine the computations
--    between nodes.
-- 
-- For instance, `(0, True, const 1, (+))` is a tree algebra that allows to
-- count the number of nodes in the tree:
--
-- 1. `0` indicates that `Empty` is not a node;
-- 2. `True` indicates that internal nodes should be taken into account;
-- 3. `const 1` indicates that each node contributes for 1 in the computation,
--    whatever its content;
-- 4. `(+)` indicates that the results must be summed up.
type TreeAlgebra a b = (b, Bool, a -> b, b -> b -> b)

-- | Folding a tree according to a given algebra
--
-- >>> let numNodesAlgebra = (0, True, const 1, (+))
-- >>> foldTree numNodesAlgebra (Leaf Void Xp)
-- 1
foldTree :: TreeAlgebra a b -> Tree a -> b
foldTree (z,_,_,_) Empty              = z
foldTree (_,_,f,_) (Leaf x _)         = f x
foldTree (z,i,f,g) int@(Internal x _ arx)
    = foldr g z' [foldTree (z,i,f,g) t | t <- elems arx]
    where z' = if i || degree int == 1 then f x else z

----------------
-- Statistics --
----------------

-- | General statistics on tree polycubes
class TreePolycube t where
    -- | Returns the number of nodes in a tree polycube
    numNodes :: t -> Int
    -- | Returns the number of leaves in a tree polycube
    numLeaves :: t -> Int
    -- | Returns the degree distribution of a tree polycube
    degreeDistribution :: t -> [Int]
    -- | Indicates if the tree polycube is final
    isFinal :: t -> Bool
    -- | Returns the degree distribution of a tree polycube
    internalDistribution :: t -> [Int]
    internalDistribution = filter (>1) . degreeDistribution

-- | A generic tree represents a tree polycube up to translation
--
-- >>> numNodes Empty
-- 0
-- >>> numNodes (Leaf Void Xp)
-- 1
-- >>> let children = array unitBounds $ (zip units (map (Leaf Void . opposite) units))
-- >>> numNodes (Internal Void Xp children)
-- 7
-- >>> numLeaves Empty
-- 0
-- >>> numLeaves (Leaf Void Xp)
-- 1
-- >>> let children = array unitBounds $ (zip units (map (Leaf Void . opposite) units))
-- >>> numLeaves (Internal Void Xp children)
-- 6
instance TreePolycube (Tree a) where
    numNodes = foldTree (0, True, const 1, (+))
    numLeaves = foldTree (0, False, const 1, (+))
    isFinal Empty             = True
    isFinal (Leaf _ _)        = True
    isFinal (Internal _ u ar) = (not . isLeaf) (ar ! u)
    degreeDistribution t = sort $ foldr (:) [] (degreeTree t)

--------------------
-- Tree polycubes --
--------------------

-- | Returns one of the basic trees in 3D
--
-- A basic tree is a tree having at most one internal node. The names of the
-- trees give a hint about their shape.
--
-- >>> numNodes $ basicTree "5opp"
-- 6
-- >>> numLeaves $ basicTree "5opp"
-- 5
basicTree :: String -> Tree Void
basicTree s = case s of
    "0"         -> makeBasic []
    "1"         -> makeBasic [Xp]
    "2flat"     -> makeBasic [Xp,Xm]
    "2corner"   -> makeBasic [Xp,Yp]
    "3flatadj"  -> makeBasic [Xp,Xm,Yp]
    "3flatopp"  -> makeBasic [Xp,Yp,Ym]
    "3pod"      -> makeBasic [Xp,Yp,Zp]
    "4flat"     -> makeBasic [Xp,Xm,Yp,Ym]
    "4podadj"   -> makeBasic [Xp,Xm,Yp,Zp]
    "4podopp"   -> makeBasic [Xp,Yp,Ym,Zp]
    "5adj"      -> makeBasic [Xp,Xm,Yp,Ym,Zp]
    "5opp"      -> makeBasic [Xp,Yp,Ym,Zp,Zm]
    "6"         -> makeBasic [Xp,Xm,Yp,Ym,Zp,Zm]
    "35"        -> representative $ head $ (makeFreeTree $ basicTree "3flatopp")
                              `freeGrafts` (makeFreeTree $ basicTree "5adj")
    "antenna"   -> representative $ head $ (makeFreeTree $ basicTree "35")
                              `freeGrafts` (makeFreeTree $ basicTree "5adj")
    "4355"      -> representative $ (!!2) $ (makeFreeTree $ basicTree "4flat")
                              `freeGrafts` (makeFreeTree $ basicTree "antenna")
    "44355"     -> representative $ (!!0) $ (makeFreeTree $ basicTree "4flat")
                              `freeGrafts` (makeFreeTree $ basicTree "4355")
    "t-antenna" -> representative $ (!!13) $ [makeFreeTree $ basicTree "4flat"]
                              `iteratedFreeGrafts` [makeFreeTree $ basicTree "44355"]
    "4t-antenna" -> representative $ (!!1) $ (makeFreeTree $ basicTree "4flat")
                              `freeGrafts` (makeFreeTree $ basicTree "t-antenna")
    otherwise   -> error "Non recognized basic tree"
    where makeBasic dirs = Internal Void Xp $
                           array unitBounds (zip units (repeat Empty))
                                         // (zip dirs  (map (Leaf Void . opposite) dirs))

-- | Returns all basic trees in 3D
--
-- >>> [numNodes t | t <- basicTrees]
-- [3,3,4,4,4,5,5,5,6,6,7]
-- >>> [numLeaves t | t <- basicTrees]
-- [2,2,3,3,3,4,4,4,5,5,6]
basicTrees :: [Tree Void]
basicTrees = [basicTree s | s <- ["2flat", "2corner", "3flatadj",
                                  "3flatopp", "3pod", "4flat", "4podadj",
                                  "4podopp", "5adj", "5opp", "6"]]

-- | Replaces the content of a tree so that it reflects the position of the
-- cells
--
-- >>> cellsTree zeroVector $ basicTree "2flat"
-- Internal [0,0,0] Xp (array (Xp,Zm) [(Xp,Leaf [1,0,0] Xm),(Xm,Leaf [-1,0,0] Xp),(Yp,Empty),(Ym,Empty),(Zp,Empty),(Zm,Empty)])
cellsTree :: Vector -> Tree a -> Tree Vector
cellsTree v Empty             = Empty
cellsTree v (Leaf _ u)        = Leaf v u
cellsTree v (Internal _ u ar) = Internal v u ar'
    where ar' = array unitBounds [(u, cellsTree v' t) | (u,t) <- assocs ar,
                                        let v' = v + vectorFromUnit u]

-- | Returns the cells occupied by the given tree
--
-- >>> (cells . cellsTree zeroVector) (basicTree "5adj")
-- [[0,0,0],[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1]]
cells :: Tree Vector -> [Vector]
cells t = foldr (:) [] t

-- | Returns the cells occupied by the given tree if root is at origin
--
-- >>> cellsO (basicTree "antenna")
-- [[0,0,0],[0,-1,0],[0,0,1],[1,0,1],[-1,0,1],[0,1,1],[0,0,2],[0,0,-1],[1,0,-1],[-1,0,-1],[0,1,-1],[0,0,-2]]
cellsO :: Tree a -> [Vector]
cellsO = cells . cellsTree zeroVector

-- | Returns the cells in this tree without the leaf pointed by the root
--
-- >>> (cellsWithoutRootLeaf . cellsTree zeroVector) (basicTree "6")
-- [[0,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]]
cellsWithoutRootLeaf :: Tree Vector -> [Vector]
cellsWithoutRootLeaf t@(Internal _ u ar)
    = filter (/= rootContent (ar ! u)) (cells t)
cellsWithoutRootLeaf otherwise
    = error "The tree does not have a root leaf"

-- | Checks if a given tree really keep track of all its neighbors
--
-- Note: This strategy is used to detect geometric cycles, i.e., cells that are
-- geometrically neighbors of other cells, but not structurally neighbors.
--
-- >>> let basicVectorTrees = map (cellsTree zeroVector) basicTrees
-- >>> and [consistentNeighborhood t (Set.fromList $ cells t) | t <- basicVectorTrees]
-- True
consistentNeighborhood :: Tree Vector -> Set Vector -> Bool
consistentNeighborhood t s = case t of
    Empty           -> True
    Leaf x u        -> leafCons x u
    Internal x u ar -> intCons x u ar
    where leafCons x u    = and [noCell x ui u | ui <- units]
          intCons  x u ar = and [ar ! ui /= Empty || noCell x ui u | ui <- units]
                         && and [consistentNeighborhood (ar ! ui) s | ui <- units]
          noCell x ui u   = ui == u || (x + vectorFromUnit ui) `Set.notMember` s

-- | Checks if the given tree is really a tree
--
-- There are two things to check:
--
-- 1. We must make sure that there are no overlapping cells. In other words,
--    the number of geometric cells must be the same as the number of nodes in
--    the tree.
-- 2. We must make sure that there is no cell having a geometric neighbor which
--    is not a structural neighbor.
--
-- >>> and [isReallyAcyclic t | t <- basicTrees]
-- True
isReallyAcyclic :: Tree a -> Bool
isReallyAcyclic t = Set.size cellsSet == length cellsList
                 && consistentNeighborhood vt cellsSet
    where vt        = cellsTree zeroVector t
          cellsList = cells vt
          cellsSet  = Set.fromList cellsList

--
-- | Returns the hull of a vector tree
--
-- >>> (hull . cellsO) (basicTree "6")
-- fromList [[-1,0,0],[0,-1,0],[0,0,-1],[0,0,0],[0,0,1],[0,1,0],[1,0,0]]
hull :: [Vector] -> Set Vector
hull = interior . extension

-------------------------
-- Free tree polycubes --
-------------------------

-- | Applies an isometry on a tree
--
-- >>> let t = basicTree "5opp"
-- >>> t
-- Internal Void Xp (array (Xp,Zm) [(Xp,Leaf Void Xm),(Xm,Empty),(Yp,Leaf Void Ym),(Ym,Leaf Void Yp),(Zp,Leaf Void Zm),(Zm,Leaf Void Zp)])
-- >>> let f = CubeIsometry [2,0,1] [-1,1,-1]
-- >>> applyIsometryOnTree f t
-- Internal Void Yp (array (Xp,Zm) [(Xp,Leaf Void Xm),(Xm,Leaf Void Xp),(Yp,Leaf Void Ym),(Ym,Empty),(Zp,Leaf Void Zm),(Zm,Leaf Void Zp)])
-- >>> and [numNodes (applyIsometryOnTree f t) == 6 | f <- cubeIsometries]
-- True
applyIsometryOnTree :: CubeIsometry -> Tree a -> Tree a
applyIsometryOnTree _ Empty             = Empty
applyIsometryOnTree f (Leaf x u)        = Leaf x (applyIsometry f u)
applyIsometryOnTree f (Internal x u ar) = Internal x (applyIsometry f u) ar'
    where ar' = array unitBounds [((applyIsometry f) ui, (applyIsometryOnTree f) (ar ! ui))
                                  | ui <- units]

-- | A free tree, i.e. a tree up to isometry
--
-- The default constructor should not be used directly. See `makeFreeTree`.
data FreeTree a = FreeTree [Tree a]

-- | Free trees are tree polycubes up to isometry
instance Ord a => TreePolycube (FreeTree a) where
    numNodes = numNodes . representative
    numLeaves = numLeaves . representative
    isFinal = isFinal . representative
    degreeDistribution = degreeDistribution . representative

-- | A more compact show
--
-- >>> makeFreeTree $ basicTree "5opp"
-- FreeTree of order 6
instance Ord a => Show (FreeTree a) where
    show ft = "FreeTree of order " ++ show (length $ orbit ft)

-- | The equality relation is induced by the representative
--
-- >>> makeFreeTree (basicTree "5opp") == makeFreeTree (basicTree "6")
-- False
-- >>> makeFreeTree (basicTree "5opp") == makeFreeTree (basicTree "5opp")
-- True
instance Ord a => Eq (FreeTree a) where
    ft1 == ft2 = representative ft1 == representative ft2

-- | The order relation is induced by the representative
--
-- >>> makeFreeTree (basicTree "5opp") < makeFreeTree (basicTree "6")
-- True
instance Ord a => Ord (FreeTree a) where
    ft1 `compare` ft2 = representative ft1 `compare` representative ft2

-- | Creates a free tree from a given tree
--
-- >>> makeFreeTree (basicTree "3pod")
-- FreeTree of order 24
makeFreeTree :: Tree a -> FreeTree a
makeFreeTree t = FreeTree [applyIsometryOnTree f t | f <- cubeIsometries]

-- | Returns the representative of a free tree
--
-- More precisely, since the relation "being isomorphic" is a relation
-- equivalence, each free tree can be assigned a unique representative. We
-- choose the minimum tree in its class, using the order on trees.
representative :: Ord a => FreeTree a -> Tree a
representative (FreeTree ts) = minimum ts

-- | Returns the orbit of a free tree, i.e. all isomorphic trees
--
-- It is well-known that the size of the orbits must divide 48.
--
-- >>> and [48 `mod` (length $ orbit t) == 0 | t <- map makeFreeTree basicTrees]
-- True
orbit :: Ord a => FreeTree a -> [Tree a]
orbit (FreeTree ts) = nub ts

-- | Returns True if the first tree is substitutable by the second
--
-- A tree @T@ is substitutable by another tree @T'@ if
--
-- 1. @T'@ is smaller than @T@;
-- 2. @T'@ is a subset of the hull of @T@, after having removed from each of
-- them the leaf root, i.e., the leaf pointed by the root.
--
-- >>> let b5    = makeFreeTree $ basicTree "5opp"
-- >>> let b4355 = makeFreeTree $ basicTree "4355"
-- >>> b4355 `isSubstitutableBy` b5
-- False
isSubstitutableBy :: Ord a => FreeTree a -> FreeTree a -> Bool
isSubstitutableBy ft1 ft2 = any (representative ft1 `isSubstitutableBy'`) (orbit ft2)
    where isSubstitutableBy' Empty _                 = False
          isSubstitutableBy' _ Empty                 = False
          isSubstitutableBy' (Leaf _ ux) (Leaf _ uy) = ux == uy
          isSubstitutableBy' (Leaf _ _) _            = False
          isSubstitutableBy' _ (Leaf _ _)            = False
          isSubstitutableBy' t1@(Internal _ ux _) t2@(Internal _ uy _)
              = ux == uy &&
                Set.fromList (cellsWithoutRootLeaf $ cellsTree zeroVector t2)
                    `Set.isSubsetOf`
                (hull $ cellsWithoutRootLeaf $ cellsTree zeroVector t1)

------------
-- Grafts --
------------

-- | Returns the graft of two trees
--
-- If the resulting tree is not acyclic, it returns `Nothing`.
--
-- >>> basicTree "2flat" `graft` basicTree "6"
-- Just (Internal Void Xp (array (Xp,Zm) [(Xp,Leaf Void Xm),(Xm,Internal Void Xp (array (Xp,Zm) [(Xp,Empty),(Xm,Leaf Void Xp),(Yp,Leaf Void Ym),(Ym,Leaf Void Yp),(Zp,Leaf Void Zm),(Zm,Leaf Void Zp)])),(Yp,Empty),(Ym,Empty),(Zp,Empty),(Zm,Empty)]))
-- >>> basicTree "6" `graft` basicTree "6"
-- Nothing
graft :: Tree a -> Tree a -> Maybe (Tree a)
graft tx@(Internal x ux arx) ty@(Internal y uy ary)
    | not compatible       = Nothing
    | not degreeCompatible = Nothing
    | isReallyAcyclic tx'  = Just tx'
    | otherwise            = Nothing
    where tx'              = Internal x ux $ arx // [(uy', ty')]
          ty'              | degree ty == 1 = Leaf y uy
                           | otherwise      = Internal y uy $ ary // [(uy, Empty)]
          uy'              = opposite uy
          compatible       = isLeaf (arx ! uy')
          degreeCompatible = degree tx + degree ty <= 8
graft _ _ = Nothing

-- | Returns all possible grafts between two free trees
--
-- For instance, there are two possible free trees obtained by grafting a
-- "2flat" tree with a "6" tree, depending on the direction of the root:
--
-- >>> makeFreeTree (basicTree "2flat") `freeGrafts` makeFreeTree (basicTree "6")
-- [FreeTree of order 6,FreeTree of order 6]
freeGrafts :: Ord a => FreeTree a -> FreeTree a -> [FreeTree a]
freeGrafts lft rft = (nub . map makeFreeTree . catMaybes) 
                     (graft <$> [representative lft] <*> orbit rft)

-- | Returns all possible iterated grafts between a free tree and a list of
-- free trees.
--
-- This is a way of generating all possible free trees of given height. More
-- precisely, if the given list contain all possible trees of height at most
-- `h`, then grafting all basic trees with all trees in this list yield all
-- possible trees of height at most `h + 1`.
--
-- >>> [makeFreeTree (basicTree "2flat")] `iteratedFreeGrafts` [makeFreeTree (basicTree "2flat")]
-- [FreeTree of order 6,FreeTree of order 6,FreeTree of order 6,FreeTree of order 6,FreeTree of order 6]
iteratedFreeGrafts :: Ord a => [FreeTree a] -> [FreeTree a] -> [FreeTree a]
iteratedFreeGrafts lfts rfts
    = lfts ++ one ++ two ++ three ++ four ++ five
    where one   = nub $ concat $ freeGrafts <$> lfts  <*> rfts
          two   = nub $ concat $ freeGrafts <$> one   <*> rfts
          three = nub $ concat $ freeGrafts <$> two   <*> rfts
          four  = nub $ concat $ freeGrafts <$> three <*> rfts
          five  = nub $ concat $ freeGrafts <$> four  <*> rfts

-------------------------
-- Abundant and sparse --
-------------------------

-- | The "conjectured" leaf function for tree polycubes.
--
-- >>> [lcub n | n <- [2..20]]
-- [2,2,3,4,5,6,6,6,7,8,9,10,10,11,11,12,13,14,14]
lcub :: Int -> Int
lcub n
    | n `elem` [6,7,13,19,25] = fcub (n + 1)
    | n <= 40                 = fcub n
    | n <= 81                 = fcub (n - 41) + 28
    | otherwise               = lcub (n - 41) + 28
    where fcub :: Int -> Int
          fcub n
              | n <= 11 = (2 * n + 2) `div` 3
              | n <= 27 = (2 * n + 3) `div` 3
              | n <= 40 = (2 * n + 4) `div` 3

-- | The delta function derived from `lcub`.
--
-- More precisely, `deltaLcub i` indicates the minimum number of leaves that
-- are destroyed when removing `i` cells from any large enough tree-like
-- polycube.
--
-- >>> [deltaLcub n | n <- [0..20]]
-- [0,0,1,2,2,3,4,4,5,6,6,7,8,8,9,10,10,11,12,12,13]
deltaLcub :: Int -> Int
deltaLcub i = minimum [lcub (n + i) - lcub n | n <- [200..300]]

-- | Returns True if a tree is sparse with respect to the given branches
--
-- >>> let b4 = makeFreeTree $ basicTree "4flat"
-- >>> let b1 = makeFreeTree $ basicTree "1"
-- >>> isSparse b4 b1
-- True
-- >>> let b5 = makeFreeTree $ basicTree "5opp"
-- >>> isSparse b5 b1
-- False
isSparse :: Ord a => FreeTree a -> FreeTree a -> Bool
isSparse branch certificate = (deltaLcub (numNodes branch - numNodes certificate))
                                     >= (numLeaves branch - numLeaves certificate)
                              &&  branch `isSubstitutableBy` certificate

-- | A branch is sparse if it can be substituted by another smaller abundant
-- branch.
--
-- The second free tree is called a certificate.
newtype Sparse a = Sparse (FreeTree a,FreeTree a)
-- | A branch is abundant if it is not sparse
newtype Abundant a = Abundant { ftFromAb :: FreeTree a }

-- | Returns the abundant and minimally sparse branches of given height
--
-- More precisely, it returns an ordered pair of lists @(A,S)@, where @A@ is
-- the list of abundant branches of given height, and @S@ is the list of all
-- sparse branches whose proper branches are abundant.
--
-- >>> [length $ abundantBranches i | i <- [0..2]]
-- [1,3,9]
abundantSparseBranches :: Int -> ([Abundant Void], [Sparse Void])
abundantSparseBranches 0 = ([Abundant $ makeFreeTree $ basicTree "1"], [])
abundantSparseBranches n = (map makeAbundant $ fst both,
                            map makeSparse $ snd both)
    where both           = partition (isNothing . snd) branches
          branches       = map substitute allGrafts
          substitute t   = (t, find (isSparse t) candidates)
          candidates     = concat [abundantBranches i | i <- [0..n-1]]
          smallAbundants = concat [abundantBranches i | i <- [1..n-1]]
          allGrafts      = (concat $ freeGrafts <$> freeBasicTrees <*> previous)
                           `iteratedFreeGrafts` smallAbundants
          freeBasicTrees = map makeFreeTree basicTrees
          previous       = nonFinalAbundantBranches (n - 1)
          makeAbundant (t,_)     = Abundant t
          makeSparse (t,Just t') = Sparse (t,t')

-- | Returns the abundant branches of given height
abundantBranches :: Int -> [FreeTree Void]
abundantBranches = map ftFromAb . fst . abundantSparseBranches

-- | Returns the final and abundant branches of given height
finalAbundantBranches :: Int -> [FreeTree Void]
finalAbundantBranches = filter isFinal . abundantBranches

-- | Returns the nonfinal and abundant branches of given height
nonFinalAbundantBranches :: Int -> [FreeTree Void]
nonFinalAbundantBranches = filter (not . isFinal) . abundantBranches

---------------
-- Saturated --
---------------

-- | A non saturated branch
--
-- The second tree is a certificate confirming why the branch is not saturated,
-- i.e., another branch that could replace the first branch and then
-- contradicting the leaf function.
newtype NonSaturated a = NonSaturated (FreeTree a, FreeTree a)
    deriving (Show)

-- | A minimally saturated branch.
--
-- A minimally saturated branch is a branch that can neither be proved non
-- saturated nor be substituted by a smaller saturated branch.
newtype MinimallySaturated a = MinimallySaturated { ftFromMinSat :: FreeTree a }
    deriving (Show)

-- | A recursively saturated branch
--
-- A branch is called recursively saturated if it can be substituted by a
-- smaller saturated branch and if the difference of leaves and number of cells
-- is consistent with the leaf function.
newtype RecursivelySaturated a = RecursivelySaturated (FreeTree a, MinimallySaturated a)
    deriving (Show)

-- | The upper line function for @lcub@
lcubUpper :: Int -> Rational
lcubUpper n = (28 * (toRational n) + 36) / 41

-- | The delta function for saturated tree polycubes
deltaLsat :: Int -> Int
deltaLsat k = 28 * q + lcub(69) - lcub(69 - r)
    where (q,r) = k `divMod` 41

-- | Indicates if the given tree is saturated
--
-- A saturated tree is a tree maximizing the ratio `numLeaves / numNodes`.
-- 
-- >>> isSaturated $ makeFreeTree $ basicTree "6"
-- True
-- >>> isSaturated $ makeFreeTree $ basicTree "4t-antenna"
-- False
isSaturated :: Ord a => FreeTree a -> Bool
isSaturated ft = toRational l >= lcubUpper n
    where n = numNodes ft
          l = numLeaves ft

-- | Indicates if the given tree is non saturated with respect to another tree.
--
-- A tree is non saturated if it can be substituted by another smaller tree
-- that would increase the ratio.
-- 
-- >>> isNonSaturated (makeFreeTree $ basicTree "4flat") (makeFreeTree $ basicTree "1")
-- True
isNonSaturated :: Ord a => FreeTree a -> FreeTree a -> Bool
isNonSaturated branch certificate
    = (deltaLsat (numNodes branch - numNodes certificate))
              > (numLeaves branch - numLeaves certificate)
    &&  branch `isSubstitutableBy` certificate

-- | Indicates if the given tree is recursively saturated.
--
-- A tree is recursively saturated if it is saturated, and if it can be
-- substituted by another smaller tree that is also saturated.
--
-- To simplify the implementation, the only recursively saturated tree
-- considere is "4t-antenna".
isRecursivelySaturated :: FreeTree Void -> FreeTree Void -> Bool
isRecursivelySaturated branch certificate
    = branch == (makeFreeTree $ basicTree "4t-antenna")

-- | Returns a partition of all "interesting" branches of given height
-- according to their "saturated" status.
--
-- There are three possible status:
--
-- 1. @MinimallySaturated@, i.e., we cannot prove that it is non saturated, but
--    we know that it cannot be substituted by a smaller saturated branch.
-- 2. @RecursivelySaturated@, i.e. the 4t-antenna.
-- 3. @NonSaturated@, i.e. we can prove that this branch does not appear in
--    large enough saturated tree-like polycubes.
branchesBySaturation :: Int -> ([MinimallySaturated Void],
                                [RecursivelySaturated Void],
                                [NonSaturated Void])
branchesBySaturation 0 = ([MinimallySaturated $ makeFreeTree $ basicTree "1"], [], [])
branchesBySaturation n = (map makeMinSat $ fst partMinOrRec,
                          map makeRecSat $ snd partMinOrRec,
                          map makeNonSat $ snd partSatNonSat)
    where partMinOrRec     = partition (isNothing . snd) saturated
          saturated        = map satSubstitute $ map fst $ fst partSatNonSat
          satSubstitute t  = (t, find (isRecursivelySaturated t) smallMinSat)
          partSatNonSat    = partition (isNothing . snd) branches
          branches         = map substitute allGrafts
          substitute t     = (t, find (isNonSaturated t) candidates)
          candidates       = concat [nonFinalMinimallySaturatedBranches i | i <- [0..n-1]]
          smallMinSat      = concat [nonFinalMinimallySaturatedBranches i | i <- [1..n-1]]
          allGrafts        = (concat $ freeGrafts <$> freeBasicTrees <*> previous)
                             `iteratedFreeGrafts` smallMinSat
          freeBasicTrees   = map makeFreeTree basicTrees
          previous         = nonFinalMinimallySaturatedBranches (n - 1)
          makeMinSat (t,_) = MinimallySaturated t
          makeRecSat (t,Just t') = RecursivelySaturated (t,MinimallySaturated t')
          makeNonSat (t,Just t') = NonSaturated (t,t')

-- | Returns the minimally saturated branches of given height
minimallySaturatedBranches :: Int -> [FreeTree Void]
minimallySaturatedBranches = map ftFromMinSat . tfst . branchesBySaturation

-- | Returns the nonfinal and saturated branches of given height
nonFinalMinimallySaturatedBranches :: Int -> [FreeTree Void]
nonFinalMinimallySaturatedBranches = filter (not . isFinal) . minimallySaturatedBranches

-- | Returns the small saturated branches.
--
-- A saturated branch is called small if its size is at most 25. Note that all
-- but one of these branches have a degree distribution whose values are in
-- `{1,2,6}`.
smallSaturated :: [FreeTree Void]
smallSaturated = filter isSaturated $ Set.toList $ Set.fromList $ [sat5,sat6] ++ all62
    where sats2       = [makeFreeTree $ basicTree sat2 | sat2 <- ["2flat","2corner"]]
          sat5        = makeFreeTree $ basicTree "5opp"
          sat6        = makeFreeTree $ basicTree "6"
          sats62      = concat [sat6 `freeGrafts` sat2 | sat2 <- sats2]
          all62       = concat $ take 8 $
                        iterate (filter not22 . allFreeGrafts sats62) sats62
          allFreeGrafts fts1 fts2 = concat [ft1 `freeGrafts` ft2 | ft1 <- fts1,
                                                                   ft2 <- fts2]
          not22 ft    = num2 ft <= num6 ft + 4
          dist ft     = internalDistribution ft
          num2 ft     = length $ filter (==2) (dist ft)
          num6 ft     = length $ filter (==6) (dist ft)

-----------------
-- JSON output --
-----------------

-- | Simple JSON conversion for serialization.
instance ToJSON Vector where
    toJSON (Vector v) = toJSON v

-- | JSON conversion for serialization.
--
-- We save the representative of the free tree, together with some useful
-- statistics (number of nodes, number of leaves, etc.).
instance Ord a => ToJSON (FreeTree a) where
    toJSON ft =
        object [ "cells"     .= cs
               , "root"      .= zeroVector
               , "direction" .= vectorFromUnit (rootUnit t)
               , "numNodes"  .= numNodes t
               , "numLeaves" .= numLeaves t
               , "degrees"   .= internalDistribution t
               , "final"     .= isFinal ft
             ]
        where t  = representative ft
              ct = cellsTree zeroVector t
              cs = cells ct

-- | JSON conversion for serialization.
--
-- Abundant branches are saved simply as free trees.
instance Ord a => ToJSON (Abundant a) where
    toJSON (Abundant ft) = toJSON ft

-- | JSON conversion for serialization.
--
-- Sparse branches are saved together with a certificate, i.e. another branch
-- that proves the sparsity property.
instance Ord a => ToJSON (Sparse a) where
    toJSON (Sparse (ft, certificate)) =
        object [ "sparse-tree"   .= toJSON ft
               , "substitute-by" .= toJSON certificate
             ]

-- | JSON conversion for serialization.
--
-- Minimally saturated branches are simply saved as free trees.
instance Ord a => ToJSON (MinimallySaturated a) where
    toJSON (MinimallySaturated ft) = toJSON ft

-- | JSON conversion for serialization.
--
-- Recursively saturated branches are saved together with a certificate, i.e.
-- another saturated branch that proves the recursively saturated property.
instance Ord a => ToJSON (RecursivelySaturated a) where
    toJSON (RecursivelySaturated (ft, certificate)) =
        object [ "recursively-saturated" .= toJSON ft
               , "substitute-by"         .= toJSON certificate
             ]

-- | JSON conversion for serialization.
--
-- Non saturated branches are saved together with a certificate, i.e. another
-- branch that, if substituing, would contradict the leaf function.
instance Ord a => ToJSON (NonSaturated a) where
    toJSON (NonSaturated (ft, certificate)) =
        object [ "non-saturated-tree" .= toJSON ft
               , "substitute-by"      .= toJSON certificate
             ]
